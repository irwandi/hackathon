
#include <ctype.h>
#include "EmonLib.h"                   // Include Emon Library
EnergyMonitor emon1;                   // Create an instance

#define RELAY1  8
#define RELAY2  9
#define RELAY3  10
#define RELAY4  11

String incomingByte = "";   // for incoming serial data
int cnt = 0;

void setup() {
  Serial.begin(9600);


  pinMode(RELAY1, OUTPUT);       
  pinMode(RELAY2, OUTPUT);   
  pinMode(RELAY3, OUTPUT);       
  pinMode(RELAY4, OUTPUT);  

  // Matiin Semua Relay
  digitalWrite(RELAY1,1);
  digitalWrite(RELAY2,1);
  digitalWrite(RELAY3,1);
  digitalWrite(RELAY4,1);
  
  emon1.current(1, 111.1);             // Current: input pin, calibration.
}

boolean isValidNumber(String str){
  for(byte i=0;i<str.length();i++) {
    if(!isDigit(str.charAt(i))) {
        return false;
    }
  }
  return true;
}  

void saklar( int num, char mod ) {
  int mode = 0;
  
  if( mod == '0' ) mode = 0;
  if( mod == '1' ) mode = 1;
  
  switch (num) {
    case 1:
      digitalWrite(RELAY1,mode); 
      break;
    case 2:
      digitalWrite(RELAY2,mode); 
      break;
    case 3:
      digitalWrite(RELAY3,mode); 
      break;
    case 4:
      digitalWrite(RELAY4,mode); 
      break;
  }
}

void loop() {

  if (Serial.available() > 0) {
    // read the incoming byte:
    incomingByte = Serial.readStringUntil('\n');

    if( isValidNumber(incomingByte) ) {
      for(byte i=0;i < incomingByte.length(); i++) {
        char tmp = incomingByte.charAt(i);
        if( tmp == '0' || tmp == '1' ) {
            saklar(i+1,tmp);
        }
      }
    }
    else {
      Serial.println(incomingByte);
    }
  }
  
  // Feed Sensor Asap
  float vol;
  int sensorValue = analogRead(A0);
  vol=(float)sensorValue/1024*5.0;
  Serial.print(vol,1);
  Serial.print(",");
  
  // Feed Sensor Arus
  double Irms = emon1.calcIrms(1480);  // Calculate Irms only
  
  Serial.print(Irms-220);		       // Irms
  
}
